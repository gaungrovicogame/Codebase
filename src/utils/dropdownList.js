const typeList = [
  { label: 'Digital Platform', value: 'Digital Platform' },
  { label: 'Digital Service', value: 'Digital Service' },
];

const areaList = [
  { value: 'Business Model', label: 'Business Model' },
  { value: 'Business Operation', label: 'Business Operation' },
  { value: 'Customer Experience', label: 'Customer Experience' },
  { value: 'Employee Experience', label: 'Employee Experience' },
  { value: 'Platform', label: 'Platform' },
  { value: 'Product/ Service', label: 'Product/ Service' },
];

const customerList = [
  { value: 'Enterprise', label: 'Enterprise' },
  { value: 'Government', label: 'Government' },
  { value: 'Telkom Group', label: 'Telkom Group' },
  { value: 'Retail/ Consumer', label: 'Retail/ Consumer' },
  { value: 'Small & Medium Business', label: 'Small & Medium Business' },
  { value: 'Wholesale', label: 'Wholesale' },
  { value: 'Other', label: 'Other' },
];

const businessFieldList = [
  { value: 'Advertising', label: 'Advertising' },
  { value: 'Agriculture', label: 'Agriculture' },
  { value: 'AR/ VR', label: 'AR/ VR' },
  { value: 'Big Data/ AI', label: 'Big Data/ AI' },
  { value: 'Blockchain', label: 'Blockchain' },
  { value: 'Communication', label: 'Communication' },
  { value: 'Cyber Security', label: 'Cyber Security' },
  { value: 'E-Commerce', label: 'E-Commerce' },
  { value: 'Education', label: 'Education' },
  { value: 'Fintech', label: 'Fintech' },
  { value: 'Games', label: 'Games' },
  { value: 'Healthcare', label: 'Healthcare' },
  { value: 'Internet of Things', label: 'Internet of Things' },
  { value: 'Logistics', label: 'Logistics' },
  { value: 'Media', label: 'Media' },
  { value: 'Music', label: 'Music' },
  { value: 'Seadex', label: 'Seadex' },
  { value: 'Smart City', label: 'Smart City' },
  { value: 'Travel & Tourism', label: 'Travel & Tourism' },
];

const productTypeList = [
  { label: 'Existing Product', value: 1 },
  { label: 'New Product', value: 2 },
  { label: 'New Product Without Name', value: 3 },
];

const optionalCanvassing = [
  { label: 'Lean Canvas of Product', value: 'Lean Canvas' },
  { label: 'Business Model Canvas of Product', value: 'Business Model Canvas' },
];

const jobLevel = [
  { label: 'Junior', value: 'Junior' },
  { label: 'Medium', value: 'Medium' },
  { label: 'Senior', value: 'Senior' },
];

const decisionList = [
  { label: 'Accept', value: 'Accept' },
  { label: 'Reject', value: 'Reject' },
];

export {
  typeList,
  areaList,
  customerList,
  businessFieldList,
  productTypeList,
  optionalCanvassing,
  jobLevel,
  decisionList,
};
