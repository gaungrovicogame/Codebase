import { ROUTES } from '../configs';

const drawerListData = [
  {
    name: 'Product',
    icon: 'box',
    url: ROUTES.HOME(),
  },
  {
    name: 'Project',
    icon: 'news',
    url: `${ROUTES.PROJECTS()}?sort=-startDate`,
  },
  {
    name: 'Business Idea',
    icon: 'box',
    url: `${ROUTES.BUSINESS_IDEA()}`,
  },
  {
    name: 'Tribe Zero',
    icon: 'news',
    url: `${ROUTES.TRIBE_ZERO()}`,
  },
];

export default drawerListData;
