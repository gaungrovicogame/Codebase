import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.setBudgetTotal();
  }

  async setBudgetTotal() {
    const { values, setFieldValue } = this.props;
    let costTalent = isNaN(values.costTalent) || values.costTalent === '' ? 0 : values.costTalent;
    let costEnabler =
      isNaN(values.costEnabler) || values.costEnabler === '' ? 0 : values.costEnabler;
    let rewardForTalent =
      isNaN(values.rewardForTalent) || values.rewardForTalent === '' ? 0 : values.rewardForTalent;
    let development =
      isNaN(values.development) || values.development === '' ? 0 : values.development;
    let partnership =
      isNaN(values.partnership) || values.partnership === '' ? 0 : values.partnership;
    let operational =
      isNaN(values.operational) || values.operational === '' ? 0 : values.operational;
    let budgetTotal =
      costTalent + costEnabler + rewardForTalent + development + partnership + operational;
    await setFieldValue('budgetTotal', parseInt(budgetTotal));
  }

  render() {
    const { errors, touched, setFieldValue, setFieldTouched, handleAutosave } = this.props;

    return (
      <>
        <h3>Resource Submission - Proposed Budget</h3>
        <p className="sub-text">
          This page identify the information about budget submission of this business idea.
        </p>
        <h5 style={{ marginBottom: '0.5rem' }}>
          <strong>Talent Based</strong>
        </h5>
        <Grid className="field-group" container>
          <Field
            name="costTalent"
            render={({ field }) => (
              <FieldText
                {...field}
                data-cy="cost-talent"
                error={errors.costTalent}
                label="Cost Talent"
                onChange={async (e) => {
                  setFieldTouched('costTalent', true);
                  await setFieldValue('costTalent', parseInt(e.target.rawValue));
                  handleAutosave;
                  this.setBudgetTotal();
                }}
                placeholder="Rp."
                required
                style={{ width: '15rem' }}
                touched={touched.costTalent}
                type="rupiah"
              />
            )}
          />
          <Field
            name="costEnabler"
            render={({ field }) => (
              <FieldText
                {...field}
                data-cy="cost-enabler"
                error={errors.costEnabler}
                label="Cost Enabler"
                onChange={async (e) => {
                  setFieldTouched('costEnabler', true);
                  await setFieldValue('costEnabler', parseInt(e.target.rawValue));
                  handleAutosave;
                  this.setBudgetTotal();
                }}
                placeholder="Rp."
                required
                style={{ width: '15rem' }}
                touched={touched.costEnabler}
                type="rupiah"
              />
            )}
          />
          <Field
            name="rewardForTalent"
            render={({ field }) => (
              <FieldText
                {...field}
                data-cy="reward-talent"
                error={errors.rewardForTalent}
                label="Reward for Talent"
                onChange={async (e) => {
                  setFieldTouched('rewardForTalent', true);
                  await setFieldValue('rewardForTalent', parseInt(e.target.rawValue));
                  handleAutosave;
                  this.setBudgetTotal();
                }}
                placeholder="Rp."
                required
                style={{ width: '15rem' }}
                touched={touched.rewardForTalent}
                type="rupiah"
              />
            )}
          />
          <Field
            name="development"
            render={({ field }) => (
              <FieldText
                {...field}
                data-cy="cost-development"
                error={errors.development}
                label="Development"
                onChange={async (e) => {
                  setFieldTouched('development', true);
                  await setFieldValue('development', parseInt(e.target.rawValue));
                  handleAutosave;
                  this.setBudgetTotal();
                }}
                placeholder="Rp."
                required
                style={{ width: '15rem' }}
                touched={touched.development}
                type="rupiah"
              />
            )}
          />
        </Grid>

        <h5 style={{ marginBottom: '0.5rem' }}>
          <strong>Project Based</strong>
        </h5>
        <Grid className="field-group" container>
          <Field
            name="partnership"
            render={({ field }) => (
              <FieldText
                {...field}
                data-cy="cost-partnership"
                error={errors.partnership}
                label="Partnership"
                onChange={async (e) => {
                  setFieldTouched('partnership', true);
                  await setFieldValue('partnership', parseInt(e.target.rawValue));
                  handleAutosave;
                  this.setBudgetTotal();
                }}
                placeholder="Rp."
                required
                style={{ width: '15rem' }}
                touched={touched.partnership}
                type="rupiah"
              />
            )}
          />
          <Field
            name="operational"
            render={({ field }) => (
              <FieldText
                {...field}
                data-cy="cost-operational"
                error={errors.operational}
                label="Operational"
                onChange={async (e) => {
                  setFieldTouched('operational', true);
                  await setFieldValue('operational', parseInt(e.target.rawValue));
                  handleAutosave;
                  this.setBudgetTotal();
                }}
                placeholder="Rp."
                required
                style={{ width: '15rem' }}
                touched={touched.operational}
                type="rupiah"
              />
            )}
          />
        </Grid>
        <Field
          name="budgetTotal"
          render={({ field }) => (
            <FieldText
              {...field}
              data-cy="cost-total"
              disabled
              label="Total Budget"
              max={13}
              placeholder="Rp."
              required
              style={{ width: '15rem' }}
              type="rupiah"
            />
          )}
        />
      </>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
