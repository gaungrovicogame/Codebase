import React from 'react';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldEditor from '../../../elements/FieldEditor';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { errors, touched, setFieldValue, setFieldTouched, handleAutosave } = this.props;

    return (
      <React.Fragment>
        <h3>Executive Summary</h3>
        <p className="sub-text">
          This page identify information about content of business idea proposals to be submitted.
        </p>

        <Field
          name="background"
          render={({ field }) => (
            <FieldEditor
              {...field}
              block
              data-cy="idea-background"
              error={errors.backgroundPlain}
              label="Background"
              onBlur={() => setFieldTouched('backgroundPlain', true)}
              onChange={(e, raw) => {
                setFieldValue('backgroundPlain', raw);
                setFieldValue('background', e);
                handleAutosave;
              }}
              placeholder={placeholder.longText('background that will be developed', 700)}
              required
              touched={touched.backgroundPlain}
            />
          )}
        />

        <Field
          name="desc"
          render={({ field }) => (
            <FieldEditor
              {...field}
              block
              data-cy="idea-desc"
              error={errors.descPlain}
              label="Description of Business Idea"
              onBlur={() => setFieldTouched('descPlain', true)}
              onChange={(e, raw) => {
                setFieldValue('descPlain', raw);
                setFieldValue('desc', e);
                handleAutosave;
              }}
              placeholder={placeholder.longText('description of business idea', 700)}
              required
              touched={touched.descPlain}
            />
          )}
        />
        <Field
          name="telkomBenefit"
          render={({ field }) => (
            <FieldEditor
              {...field}
              block
              data-cy="benefit-telkom"
              error={errors.telkomBenefitPlain}
              label="Value Creation - Benefit for Telkom"
              onBlur={() => setFieldTouched('telkomBenefitPlain', true)}
              onChange={(e, raw) => {
                setFieldValue('telkomBenefitPlain', raw);
                setFieldValue('telkomBenefit', e);
                handleAutosave;
              }}
              placeholder={placeholder.longText('benefit for Telkom', 700)}
              required
              touched={touched.telkomBenefitPlain}
            />
          )}
        />

        <Field
          name="customerBenefit"
          render={({ field }) => (
            <FieldEditor
              {...field}
              block
              data-cy="benefit-customer"
              error={errors.customerBenefitPlain}
              label="Value Creation - Benefit for Customer"
              onBlur={() => setFieldTouched('customerBenefitPlain', true)}
              onChange={(e, raw) => {
                setFieldValue('customerBenefitPlain', raw);
                setFieldValue('customerBenefit', e);
                handleAutosave;
              }}
              placeholder={placeholder.longText('benefit for customer', 700)}
              required
              touched={touched.customerBenefitPlain}
            />
          )}
        />

        <Field
          name="valueCapture"
          render={({ field }) => (
            <FieldEditor
              {...field}
              block
              data-cy="value-capture"
              error={errors.valueCapturePlain}
              label="Value Capture"
              onBlur={() => setFieldTouched('valueCapturePlain', true)}
              onChange={(e, raw) => {
                setFieldValue('valueCapturePlain', raw);
                setFieldValue('valueCapture', e);
                handleAutosave;
              }}
              placeholder={placeholder.longText('value capture', 700)}
              required
              touched={touched.valueCapturePlain}
              type="long"
            />
          )}
        />
      </React.Fragment>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired
};
