import validateInput from '../../../utils/validateInput';

const validate = values => {
  const {
    email,
    password,
  } = values;
  const errors = {
    email: validateInput(email, ['required', 'email']),
    password: validateInput(password, ['required']),
    
  };

  return errors;
};


export default validate;
