import React from 'react';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import { placeholder } from '../../../../constants/copywriting';
import FieldText from '../../../elements/FieldText';
import FieldSelect from '../../../elements/FieldSelect';
import FieldDate from '../../../elements/FieldDate';
import FieldTime from '../../../elements/FieldTime';
import Grid from '@material-ui/core/Grid';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tribeList: [],
    };
  }

  componentDidMount() {
    this.getTribe();
    this.getPitchingLocation();
  }

  async getTribe() {
    await this.props.getTribe().then((res) => this.setState({ tribeList: res }));
  }

  async getPitchingLocation() {
    await this.props.getPitchingLocation();
  }

  render() {
    const { errors, touched, setFieldValue, setFieldTouched, handleAutosave, values } = this.props;

    return (
      <>
        <h3>Recommendation</h3>
        <p className="sub-text">
          This page identify the recommendation of pitching schedule and tribe from Tribe Zero
        </p>
        <Field
          name="pitchingDate"
          render={({ field }) => (
            <FieldDate
              {...field}
              data-cy="exec-date"
              dateFormat="DD/MM/YYYY"
              error={errors.pitchingDate}
              label="Pitching Schedule"
              onChange={(e) => {
                setFieldValue('pitchingDate', e);
                handleAutosave;
              }}
              placeholder="DD/MM/YYYY"
              required
              touched={touched.pitchingDate}
              useFormattedValue
            />
          )}
        />
        <Grid className="field-group" container>
          <Field
            name="pitchingTimeStart"
            render={({ field }) => (
              <FieldTime
                {...field}
                data-cy="pitching-time-start"
                error={errors.pitchingTimeStart}
                label="Start"
                onChange={async (e) => {
                  setFieldTouched('pitchingTimeStart', true);
                  await setFieldValue('pitchingTimeStart', e.target.value);
                  handleAutosave;
                }}
                placeholder="HH:MM"
                required
                touched={touched.pitchingTimeStart}
              />
            )}
          />
          <Field
            name="pitchingTimeEnd"
            render={({ field }) => (
              <FieldTime
                {...field}
                data-cy="pitching-time-end"
                error={errors.pitchingTimeEnd}
                label="Finish"
                onChange={async (e) => {
                  let value =
                    parseInt(values.pitchingTimeStart.replace(':', '')) >
                      parseInt(e.target.rawValue) && e.target.rawValue.length === 4
                      ? values.pitchingTimeStart
                      : e.target.value;
                  setFieldTouched('pitchingTimeEnd', true);
                  await setFieldValue('pitchingTimeEnd', value);
                  handleAutosave;
                }}
                placeholder="HH:MM"
                required
                touched={touched.pitchingTimeEnd}
              />
            )}
          />
        </Grid>
        <Field
          name="address"
          render={({ field }) => (
            <FieldText
              {...field}
              data-cy="place"
              error={errors.address}
              label="Place"
              onChange={(e) => {
                setFieldTouched('address', true);
                setFieldValue('address', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.shortText('place')}
              required
              touched={touched.address}
              type="long"
            />
          )}
        />

        <Grid className="field-group" container>
          <Field
            name="tribe"
            render={({ field }) => (
              <FieldSelect
                {...field}
                dataCy="tribe"
                error={errors.tribe}
                label="Tribe"
                onBlur={() => setFieldTouched('tribe', true)}
                onChange={(e) => {
                  setFieldValue('tribe', e);
                  handleAutosave;
                }}
                options={this.state.tribeList}
                placeholder={placeholder.select('tribe')}
                required
                touched={touched.tribe}
              />
            )}
            type="select"
          />
          {values.tribe === 'Other' ? (
            <Field
              name="otherTribe"
              render={({ field }) => (
                <FieldText
                  {...field}
                  data-cy="tribe-other"
                  error={errors.otherTribe}
                  onChange={(e) => {
                    setFieldTouched('otherTribe', true);
                    setFieldValue('otherTribe', e.target.value);
                    handleAutosave;
                  }}
                  placeholder={placeholder.shortText('other tribe')}
                  required
                  touched={touched.otherTribe}
                />
              )}
            />
          ) : null}
        </Grid>
      </>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  getPitchingLocation: PropTypes.func.isRequired,
  getTribe: PropTypes.func.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
