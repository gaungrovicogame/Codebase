import React from 'react';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import Dropzone from '../../../elements/Dropzone';
import { Grid } from '@material-ui/core';

export default class Component extends React.Component {
  render() {
    const {
      errors,
      touched,
      setFieldValue,
      setFieldTouched,
      handleAutosave,
      onFilesAdded,
      onFilesRemoved,
      values,
    } = this.props;

    return (
      <React.Fragment>
        <h3>Canvassing of Product - Value Proposition Canvas</h3>{' '}
        <p className="sub-text">This page identify the explanation of product canvassing. </p>
        <Grid container style={{ marginBottom: '1rem' }}>
          <Grid item lg={6} md={6} xs={12}>
            <Dropzone
              dataCy="value-prop-image"
              disabled
              error={errors.valuePropCustomerUrl}
              handleTouch={setFieldTouched}
              image={values.valuePropCustomerUrl}
              initialValues={{
                bodyText: 'Click or drag image here (format jpg/png)',
                captionText: '30 MB Maximum',
              }}
              isFailed={values.valuePropCustomer.isFailed}
              isUploaded={values.valuePropCustomer.isUploaded}
              label="Value Proposition Canvas of Product"
              name="valuePropCustomerUrl"
              onFilesAdded={(files) =>
                onFilesAdded(files, 'valuePropCustomerUrl', setFieldValue, values)
              }
              onFilesRemoved={(files) =>
                onFilesRemoved(files, 'valuePropCustomerUrl', setFieldValue, values)
              }
              progress={values.valuePropCustomer.progress}
              required
              style={{ width: '26rem' }}
              touched={touched.valuePropCustomerUrl}
            />
          </Grid>
        </Grid>
        <FieldText
          block
          data-cy="value-prop-jobs"
          disabled
          label="Customer's Job"
          placeholder={placeholder.longText(`customer's job of product`, 700)}
          required
          type="long"
          value={values.valuePropCustomerJobs}
        />
        <FieldText
          block
          data-cy="value-prop-pain"
          disabled
          label="Customer's Pain"
          placeholder={placeholder.longText(`customer's pain of product`, 700)}
          required
          type="long"
          value={values.valuePropCustomerPain}
        />
        <FieldText
          block
          data-cy="value-prop-gain"
          disabled
          label="Customer's Gain"
          placeholder={placeholder.longText(`customer's gain of product`, 700)}
          required
          type="long"
          value={values.valuePropCustomerGain}
        />
        <FieldText
          block
          data-cy="value-prop-prod-services"
          disabled
          label="Products & Services"
          placeholder={placeholder.longText('product & services of product', 700)}
          required
          type="long"
          value={values.valuePropCustomerProductService}
        />
        <FieldText
          block
          data-cy="value-prop-pain-relievers"
          disabled
          label="Pain Relievers"
          placeholder={placeholder.longText('pain relievers of product', 700)}
          required
          type="long"
          value={values.valuePropCustomerPainRelievers}
        />
        <FieldText
          block
          data-cy="value-prop-pain-gain-creators"
          disabled
          label="Gain Creators"
          placeholder={placeholder.longText('gain creators of product', 700)}
          required
          type="long"
          value={values.valuePropCustomerGainCreators}
        />
        <Field
          name="feedbackValuePropCustomer"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-value-prop-canvas"
              error={errors.feedbackValuePropCustomer}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of Value Proposition Canvas"
              onChange={(e) => {
                setFieldTouched('feedbackValuePropCustomer', true);
                setFieldValue('feedbackValuePropCustomer', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`evaluation of value proposition canvas`, 700)}
              required
              touched={touched.feedbackValuePropCustomer}
              type="long"
            />
          )}
        />
      </React.Fragment>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  onFilesAdded: PropTypes.func.isRequired,
  onFilesRemoved: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
