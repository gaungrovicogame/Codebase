import React from 'react';
import PropTypes from 'prop-types';
import Select, { components } from 'react-select';
import CreatableSelect from 'react-select/creatable';
import './styles.modules.scss';
import { Grid } from '@material-ui/core';
import ErrorMessage from '../ErrorMessage';
import { ICONS } from '../../../configs';
import Tooltip from '../Tooltip';
import Button from '../Button';

const { Placeholder } = components;

export default class FieldSelect extends React.Component {
  constructor(props) {
    super(props);
    this._getClass = this._getClass.bind(this);
    this._renderOptionalLabel = this._renderOptionalLabel.bind(this);
    this._handleDeleteItem = this._handleDeleteItem.bind(this);
    this.state = {
      value: this.props.value,
    };
  }

  componentDidMount() {
    this.updateValue();
  }

  async componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      await this.updateValue();
    }
  }

  updateValue() {
    this.setState({ value: this.props.value });
  }

  persistentPlaceholder = ({ children, ...props }) => {
    return (
      <components.ValueContainer {...props}>
        <Placeholder {...props} isFocused={props.isFocused}>
          {props.selectProps.placeholder}
        </Placeholder>
        {React.Children.map(children, (child) => {
          return child && child.type !== Placeholder ? child : null;
        })}
      </components.ValueContainer>
    );
  };

  _handleChange = (event) => {
    let data = [];
    if (this.props.multiple) {
      event.map((item) => {
        data.push(item.value);
      });
    } else {
      data = event.value;
    }
    this.setState({
      value: data,
    });
    this.props.onChange(data);
  };

  _handleDeleteItem = (i) => {
    this.state.value.splice(i, 1);
    this.setState({ value: this.state.value });
    this.props.onChange(this.state.value);
  };

  _renderOptionalLabel(required, isMultiple) {
    if (!required) {
      return <span className="optional"> (optional) </span>;
    } else if (isMultiple) {
      return <span className="optional"> (multiple) </span>;
    }

    return;
  }

  _getClass() {
    let className = 'cdx-react-select';
    if (this.props.error && this.props.touched) {
      className = className + ' error-input';
    }
    if (this.props.small) {
      className = className + ' small';
    }

    return className;
  }

  _getMainClass() {
    let className = 'cdx-select';
    if (this.props.long) {
      className = className + ' long';
    }
    if (this.props.block) {
      className = className + ' block';
    }

    return className;
  }

  render() {
    const {
      options,
      field,
      touched,
      info,
      error,
      multiple,
      disabled,
      className,
      label,
      required,
      withSearch,
      dataCy,
      creatable,
      style,
    } = this.props;

    let value = this.state.value;

    let multiValue = [];

    if (Array.isArray(value)) {
      value.map((val) => {
        let newValue = false;
        options.find((e) => {
          if (val === e.value) {
            multiValue.push(e);
            newValue = true;
            return;
          }
        });
        if (!newValue) {
          multiValue.push({
            label: val,
            value: val,
            __isNew__: true,
          });
        }
      });
    }

    const newValue = multiple ? multiValue : options.find((e) => value === e.value) || '';

    const attrs = {
      className: this._getClass(),
      classNamePrefix: 'select',
      isDisabled: disabled,
      isMulti: multiple,
      isSearchable: withSearch,
      options: options,
      value: newValue,
      onChange: (e) => this._handleChange(e),
    };

    const DropdownIndicator = () => {
      return <img className="icon-component" src={ICONS.ICON_DROPDOWN} />;
    };

    return (
      <Grid
        className={`${this._getMainClass()} ${className}`}
        data-cy={dataCy}
        direction="column"
        style={style}
      >
        <Grid align="end" className="label" container justify="space-between">
          {label !== '' ? (
            <p>
              {label}{' '}
              {info !== '' && (
                <Tooltip placement="bottom-start" title={info}>
                  <img
                    src={ICONS.ICON_TOOLTIP}
                    style={{ height: '1rem', transform: 'translate(0, 0.2rem)' }}
                  />
                </Tooltip>
              )}
            </p>
          ) : (
            <p style={{ color: '#00000000' }}>.</p>
          )}
          {this._renderOptionalLabel(required, creatable)}
        </Grid>
        {creatable ? (
          <CreatableSelect
            {...this.props}
            {...field}
            {...attrs}
            components={{
              DropdownIndicator,
              ValueContainer: this.persistentPlaceholder,
            }}
            formatCreateLabel={(input) => `Add "${input}"`}
            styles={{
              placeholder: (provided, state) => ({
                ...provided,
                position: 'absolute',
                display: state.selectProps.inputValue ? 'none' : 'inline',
                transition: 'top 0.1s, font-size 0.1s',
                fontSize: state.selectProps.inputValue && 13,
              }),
            }}
          />
        ) : (
          <Select {...this.props} {...field} {...attrs} components={{ DropdownIndicator }} />
        )}
        {touched && error && <ErrorMessage> {error} </ErrorMessage>}
        {multiple && (
          <Grid className="select-multi-container" container>
            {newValue.map((val, idx) => (
              <Grid className="select-multi-item" item key={idx}>
                <p>{val.label}</p>
                {!disabled && (
                  <Button onClick={() => this._handleDeleteItem(idx)} small>
                    <img src={ICONS.CLOSE_MINI_WHITE} />
                  </Button>
                )}
              </Grid>
            ))}
          </Grid>
        )}
      </Grid>
    );
  }
}

FieldSelect.defaultProps = {
  block: false,
  className: '',
  creatable: false,
  dataCy: '',
  disabled: false,
  error: '',
  field: {},
  info: '',
  label: '',
  long: false,
  multiple: false,
  onChange: '',
  options: [],
  required: false,
  small: false,
  style: {},
  touched: false,
  value: '',
  withSearch: false,
};

FieldSelect.propTypes = {
  block: PropTypes.bool,
  className: PropTypes.string,
  creatable: PropTypes.bool,
  dataCy: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  field: PropTypes.object,
  info: PropTypes.string,
  label: PropTypes.string,
  long: PropTypes.bool,
  multiple: PropTypes.bool,
  onChange: PropTypes.func,
  options: PropTypes.array,
  required: PropTypes.bool,
  small: PropTypes.bool,
  style: PropTypes.object,
  touched: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.array]),
  withSearch: PropTypes.bool,
};
