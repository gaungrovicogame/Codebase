import React from 'react';
import renderer from 'react-test-renderer';
import Component from '../component';

describe('Field Select', () => {
  it('Normal Select Input renders correctly', () => {
    const tree = renderer.create(<Component required />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Normal Select Input renders correctly', () => {
    const tree = renderer
      .create(<Component error="this is error messages" long small touched />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Select with multiple input renders correctly', () => {
    const tree = renderer
      .create(<Component block creatable label="Autocomplete" multiple required searchable />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
