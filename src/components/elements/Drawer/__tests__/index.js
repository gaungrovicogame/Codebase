import React from 'react';
import { shallow } from 'enzyme';
import Drawer from '../index';

describe('Drawer', () => {
  it('Called when logout clicked', () => {
    const _handleLogoutClick = jest.fn();
    const wrapper = shallow(<Drawer />);

    wrapper.find('#logout-button').simulate('click');    
    
    expect(_handleLogoutClick).toBeCalled;
  });
});
