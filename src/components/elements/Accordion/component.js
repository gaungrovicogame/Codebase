import React from 'react';
import PropTypes from 'prop-types';
import { ICONS } from './../../../configs';
import styles from './styles.css';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
    };
  }

  _handleExpand = () => {
    if (this.state.open) {
      this.setState({ open: false });
    } else {
      this.setState({ open: true });
    }
  }

  _handleIconArrow() {
    return (
      <>
        <img className={styles.icon}
          src={this.state.open === true ? ICONS.ARROW_RIGHT : ICONS.ARROW_DOWN} />
      </>
    );
  }

  render() {
    const { desc, name } = this.props;
    const title = typeof (name) === 'string' ? (
      <h4 className={styles['header-title']}>{name}</h4>
    ) : name;

    return (
      <main className={this.state.open ? styles['expand-container'] : styles.container} >
        <header className={styles.header} onClick={this._handleExpand}>
          {this._handleIconArrow()}
          {title}
        </header>
        <div className={styles.border} />
        <section className={styles.content}>
          {desc}
        </section>
      </main>
    );
  }
}

Component.defaultProps = {
  desc: <h1>a</h1>,
  name: 'One Two'
};

Component.propTypes = {
  desc: PropTypes.element,
  name: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]),
};
