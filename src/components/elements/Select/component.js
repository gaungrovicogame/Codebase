import React from 'react';
import { ICONS } from './../../../configs';
import PropTypes from 'prop-types';
import Select from 'react-select';
import styles from './styles.css';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if(nextProps.value !== prevState.value && nextProps.value === '') {
      return{ value: undefined };
    } else return null;
  }

  _handleChange = (event) => {
    this.props.onChange(event);
    this.setState({ value: event.value });
  }

  render() {
    const { className, data, name, width } = this.props;
    const dataFilter = [];
    let value = this.state.value;
    let optionWidth = null;

    const customClass = className ? `${styles.select} ${className}` : styles.select;
    const capitalize = (s) => {
      if (typeof s !== 'string') return '';
      return s.charAt(0).toUpperCase() + s.slice(1);
    };

    data.map((item) => (
      dataFilter.push({ value: item.id, label: capitalize(item.name), name: name })
    ));
    const newValue = dataFilter.find(function (e) {
      return (e.value === value);
    });

    if (dataFilter.length > 5) {
      optionWidth = parseInt(width) - 17;
    }
    else {
      optionWidth = width;
    }    

    const colourStyles = {
      control: (styles) => (
        {
          ...styles,
          '&:hover': {
            cursor: 'pointer'
          },
          backgroundColor: 'white',
          border: '0.0625rem solid #707a89',
          borderRadius: '0.25rem',
          boxShadow: 'none',
          height: '2rem',
          margin: '0rem',
          minHeight: '2rem !important',
          padding: '0',
          width: '100%'
        }),
      indicatorSeparator: () => (
        {
          width: 0
        }),
      menu: (styles) => (
        {
          ...styles,
          boxShadow: '0 0.125rem 0.25rem 0 rgba(72, 122, 157, 0.5)',
          maxWidth: '27.9375rem',
          minWidth: width,
          width: width,
        }),
      menuList: (styles) => (
        {
          ...styles,
          paddingBottom: 0,
          paddingTop: 0,
        }),
      option: (styles, { isFocused, isSelected }) => {
        return {
          ...styles,
          backgroundColor:
            isSelected
              ? '#E4F7F6'
              : isFocused
                ? '#EBEBEB'
                : null,
          color: 'black',
          maxHeight: '6.25rem',
          maxWidth: optionWidth,
          '&:hover': {
            cursor: 'pointer',
          },
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap',
          width: width
        };
      },
      singleValue: (styles) => ({
        ...styles,
        margin: '0',
        padding: '0'
      }),
      valueContainer: (styles) => ({
        ...styles,
        margin: '0',
        padding: '0 0 0 0.5rem'
      })
    };

    const DropdownIndicator = () => {
      return (
        <img className={styles['icon-component']} src={ICONS.ICON_DROPDOWN} />
      );
    };

    return (
      <div className={customClass} style={{ width: `${width}` }}>
        <Select
          classNamePrefix={styles['select-style']}
          components={{ DropdownIndicator }}
          defaultValue={dataFilter[0]}
          id="select"
          isSearchable={false}
          label={name}
          maxMenuHeight={195}
          onChange={this._handleChange}
          options={dataFilter}
          styles={colourStyles}
          value={newValue}
        />
      </div>
    );
  }
}

Component.propTypes = {
  className: PropTypes.string,
  data: PropTypes.array,
  name: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  width: PropTypes.string,
};

Component.defaultProps = {
  className: '',
  data: '',
  name: '',
  onChange: '',
  value: '',
  width: '',
};
