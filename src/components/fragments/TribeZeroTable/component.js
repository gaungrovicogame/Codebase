import React from 'react';
import PropTypes from 'prop-types';
import Pagination from '../../elements/Pagination';
import styles from './styles.module.scss';
import { ICONS } from './../../../configs';
import queryString from 'query-string';
import EditIcon from '../../icons/Edit';
import DownloadIcon from '../../icons/Download';
import { Grid } from '@material-ui/core';
import stylesTable from '../../../styles/_table.module.scss';
import Tooltip from '../../elements/Tooltip';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this._handleOnChangeSchedule = this._handleOnChangeSchedule.bind(this);
  }

  _handleChangeSorting = (e) => {
    const { onChangeSort } = this.props;
    const field = e.target.getAttribute('index');
    const query = queryString.parse(location.search);

    if (query.sort) {
      let value = undefined;
      const char = query.sort.split('-');

      if (query.sort.charAt(0) === '-') {
        value = char[1] === field ? undefined : field;
      } else {
        value = char[0] === field ? `-${field}` : field;
      }

      onChangeSort({ name: 'sort', value });
    } else {
      onChangeSort({ name: 'sort', value: field });
    }
  };

  _handleIconSort(item) {
    const query = queryString.parse(location.search);
    let icon = '';

    if (query.sort) {
      const char = query.sort.split('-');
      if (query.sort.charAt(0) === '-') {
        icon = char[1] === item ? ICONS.SORT_DOWN : ICONS.ICON_SORT;
      } else {
        icon = char[0] === item ? ICONS.SORT_UP : ICONS.ICON_SORT;
      }
    } else {
      if (item === 'startDate') {
        icon = ICONS.SORT_DOWN;
      } else {
        icon = ICONS.ICON_SORT;
      }
    }

    return (
      <img
        className={styles['sort-icon']}
        index={item}
        onClick={this._handleChangeSorting}
        src={icon}
      />
    );
  }

  _handleOnEvaluate(e, id) {
    e.preventDefault();
    this.props.onEvaluateItem(id);
  }

  _handleOnChangeSchedule(e, data) {
    e.preventDefault();
    this.props.onChangeSchedule(data);
  }

  _handleOnDownload(e, id) {
    e.preventDefault();
    this.props.onDownloadItem(id);
  }

  _handleDisabledAction(actName, status) {
    // status === status Code

    let isDisabled = false;

    switch (actName) {
      case 'evaluate':
        if (status === 1) isDisabled = false;
        else isDisabled = true;
        break;
      case 'download':
        if (status !== 1) isDisabled = false;
        else isDisabled = true;
        break;
      case 'schedule':
        if (status === 2) isDisabled = false;
        else isDisabled = true;
        break;
    }

    return isDisabled;
  }

  render() {
    return (
      <>
        <table className={stylesTable['cdx-table']} id={styles['business-idea-table']}>
          <thead>
            <tr>
              <th>Name of Idea</th>
              <th>Date</th>
              <th>Status</th>
              <th>Pitching Schedule</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody className={styles.tbody}>
            {this.props.data &&
              this.props.data.map((row, index) => (
                <tr key={index}>
                  <td>
                    {row.name ? (
                      <Tooltip placement="bottom-start" title={row.name}>
                        <span>{row.name}</span>
                      </Tooltip>
                    ) : null}
                  </td>
                  <td>{row.updated_at}</td>
                  <td>{row.status}</td>
                  <td>{row.pitching_schedule}</td>
                  <td className={styles['td-action']}>
                    <Grid alignItems="baseline" container direction="row" justify="space-evenly">
                      <Grid item>
                        <button
                          disabled={this._handleDisabledAction('evaluate', row.status_code)}
                          onClick={(e) => this._handleOnEvaluate(e, row.id)}
                        >
                          <EditIcon
                            color={
                              this._handleDisabledAction('evaluate', row.status_code)
                                ? '#dee3ed'
                                : '#1E2025'
                            }
                          />
                          Evaluate
                        </button>
                      </Grid>
                      <Grid item>
                        <button
                          disabled={this._handleDisabledAction('download', row.status_code)}
                          onClick={(e) => this._handleOnDownload(e, row.id)}
                        >
                          <DownloadIcon
                            color={
                              this._handleDisabledAction('download', row.status_code)
                                ? '#dee3ed'
                                : '#1E2025'
                            }
                          />
                          Download
                        </button>
                      </Grid>
                      <Grid item>
                        <button
                          disabled={this._handleDisabledAction('schedule', row.status_code)}
                          onClick={(e) => this._handleOnChangeSchedule(e, row)}
                        >
                          <EditIcon
                            color={
                              this._handleDisabledAction('schedule', row.status_code)
                                ? '#dee3ed'
                                : '#1E2025'
                            }
                          />
                          Edit Schedule
                        </button>
                      </Grid>
                    </Grid>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {this.props.data && (
          <Pagination
            className={styles.pagination}
            meta={this.props.meta}
            onChangeUrl={this.props.onChangeUrl}
          />
        )}
      </>
    );
  }
}

Component.defaultProps = {
  match: {},
  onChangeSort: {},
  onChangeUrl: {},
};

Component.propTypes = {
  data: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object,
  meta: PropTypes.object.isRequired,
  onChangeSchedule: PropTypes.func.isRequired,
  onChangeSort: PropTypes.func,
  onChangeUrl: PropTypes.func,
  onDownloadItem: PropTypes.func.isRequired,
  onEvaluateItem: PropTypes.func.isRequired,
};
