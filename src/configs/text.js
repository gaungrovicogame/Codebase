const text = {
  TOOLTIP_CHAPTER_GIT() { return `This number is based on talent’s activity in gitlab.`; },
  TOOLTIP_LOGIN() { return `Please enter your registered Telkom NIK and password`;},
  TOOLTIP_TALENT_SCORE() { return `The score is compared by previous month.`; },
  TOOLTIP_TALENT_SCORE_STATE(data) { return `Difference: ${Math.abs(data)} point`;},
  TOOLTIP_VELOCITY_DATE() { return `This is an average number of backlog story point (velocity) and all backlog (feature) in this date range.`; },
  TOOLTIP_VELOCITY_SPRINT() { return `This is an amount of backlog story point (velocity) and all backlog (feature) in this sprint.`; },
};
  
export default text;
