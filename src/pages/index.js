import Error404 from './Error404';
import Login from './Login';
import Products from './Products';
import Projects from './Projects';
import ProjectDetails from './ProjectDetails';
import BusinessIdea from './BusinessIdea';
import TribeZero from './TribeZero';
import BusinessIdeaForm from './BusinessIdeaForm';
import TribeZeroComment from './TribeZeroComment';

const pages = {
  Error404,
  Login,
  Products,
  Projects,
  ProjectDetails,
  BusinessIdea,
  BusinessIdeaForm,
  TribeZero,
  TribeZeroComment,
};

export default pages;
