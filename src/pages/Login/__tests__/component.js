import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Component from '../component';

jest.mock('../../../components/forms/Login');
jest.mock('../../../configs');

describe('Component', () => {
  it('renders correctly', () => {
    const tree = renderer;
    expect(tree).toMatchSnapshot();
  });

  it('calls action when submit', () => {
    const actions = {
      fetchLogin: jest.fn()
    };
    const wrapper = shallow(<Component actions={actions} code={{}} />);

    wrapper.find('#loginForm').simulate('submit');
    expect(actions.fetchLogin).toHaveBeenCalled();
  });
});
