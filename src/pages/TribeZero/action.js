import fetch from '../../utils/fetch';
import { SERVICES } from '../../configs';
import { ACTIONS } from '../../constants';
import { push } from 'react-router-redux';
import moment from 'moment';
import queryString from 'query-string';
import axios from 'axios';

export function setQuery(item) {
  if (item.isDate) {
    return (dispatch) => {
      const value = queryString.parse(location.search);
      value.startDate = item.from.toISOString();
      value.endDate = item.to.toISOString();
      const urlParser = `?${queryString.stringify(value)}`;
      dispatch(push(urlParser));
    };
  } else {
    return (dispatch) => {
      const value = queryString.parse(location.search);
      value[item.name] = item.value;
      const urlParser = `?${queryString.stringify(value)}`;
      dispatch(push(urlParser));
    };
  }
}

export function getListBusinessIdea() {
  return (dispatch) => {
    dispatch({ type: ACTIONS.LOADING });

    return new Promise((resolve, reject) => {
      const value = queryString.parse(location.search);
      const urlParser = `?${queryString.stringify(value)}`;

      fetch({
        method: 'GET',
        url: SERVICES.GET_TRIBE_ZERO(urlParser),
      })
        .then((res) => {
          const transformedData = [];
          res.data.forEach((el) => {
            let status = 'Draft';
            switch (el.status) {
              case 1:
                status = 'Need Evaluation';
                break;
              case 2:
                status = 'Pitching scheduled';
                break;
              case 3:
                status = 'Evaluation Sent';
                break;
              case 4:
                status = 'Pitching';
                break;
              case 5:
                status = 'Reject';
                break;
              case 6:
                status = 'On boarding';
                break;
              case 7:
                status = 'Running';
                break;
              case 8:
                status = 'Terminate';
                break;

              default:
                break;
            }

            transformedData.push({
              ...el,
              status: status,
              status_code: el.status,
              pitching_schedule:
                el.status !== 1 && el.status !== 3
                  ? `${el['comment.page10_pitching_schedule'].date}, ${el['comment.page10_pitching_schedule'].time_start} - ${el['comment.page10_pitching_schedule'].time_end}`
                  : '-',
              updated_at: moment(el.updated_at).format('DD/MM/YYYY, HH:mm'),
            });
          });
          dispatch({
            type: ACTIONS.LIST_OF_TRIBE_ZERO_FETCHED,
            data: {
              data: transformedData,
              meta: {
                totalPage: res.meta.total_pages,
                page: res.meta.current_page,
                totalData: res.meta.total_data,
              },
            },
          });
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
}

export function downloadBusinessIdea(id) {
  return new Promise((resolve, reject) => {
    fetch({
      method: 'GET',
      url: SERVICES.GET_TRIBE_ZERO_PDF(id),
    })
      .then(() => {
        window.open(SERVICES.GET_TRIBE_ZERO_PDF(id), '_blank');
        resolve(true);
      })
      .catch(() => {
        setTimeout(() => {
          fetch({
            method: 'GET',
            url: SERVICES.GET_TRIBE_ZERO_PDF(id),
          })
            .then(() => {
              window.open(SERVICES.GET_TRIBE_ZERO_PDF(id), '_blank');
              resolve(true);
            })
            .catch((err) => reject(err));
        }, 500);
      });
  });
}

export function editPitchingSchedule(data) {
  return (dispatch) => {
    return new Promise((resolve, reject) => {
      let sendData = {
        pitching_schedule: {
          date: data.pitchingDate,
          time_start: data.pitchingTimeStart,
          time_end: data.pitchingTimeEnd,
        },
        pitching_place: data.address,
      };

      axios
        .post(SERVICES.EDIT_PITCHING_SCHEDULE(data.id), sendData)
        .then(() => {
          dispatch({ type: ACTIONS.EDIT_PITCHING_SCHEDULE });
          resolve(true);
        })
        .catch((e) => {
          reject(e);
        });
    });
  };
}
